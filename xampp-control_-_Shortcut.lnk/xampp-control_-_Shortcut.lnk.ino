#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Keypad.h>

#define Password_lenght 4

char Data[Password_lenght]; 
char Master[Password_lenght] = "123";
char Demaster[Password_lenght] = "456";

byte data_count = 0;
   
char customKey;

const byte ROWS = 4;
const byte COLS = 3;

//to determine state of the device
boolean ToDefuseSate = false;
boolean ToActivationState = true;


char hexaKeys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'},
};

byte rowPins[ROWS] = {9,8,7,6};
byte colPins[COLS]= {5,4,3}; 

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);
const int  buzzer = 11;
LiquidCrystal_I2C lcd(0x27,16,2);
 
boolean setToDefuse  = false;
boolean setToActivate  = false;

void setup() {
  // put your setup code here, to run once:
  //pinMode(13,OUTPUT);
  Serial.begin(9600);  
  lcd.backlight();
  lcd.init(); 

  pinMode(buzzer, OUTPUT);
}

void loop() {
  lcd.setCursor(0,0);
  
  customKey = customKeypad.getKey();

  if( !ToDefuseSate && ToActivationState  ){

       if(customKey == '*'){
          lcd.clear();
          clearData(); 
          delay(1000);
          setToActivate = true;
          customKey = false;
       }

       
       if( setToActivate ){
           ActivateBomb(customKey);
       } else { 
          lcd.print("Press * to Setup"  );
       }

     
      
     
  } else if ( ToDefuseSate && !ToActivationState ) {

       if(customKey == '#'){
          lcd.clear();
          clearData(); 
          delay(1000);
          setToDefuse = true;
          customKey = false;
       } 

       if( setToDefuse ){
          DefuseBomb(customKey);
       } else {
          lcd.print("Bomb is now ticking... "  );
          tone(buzzer, 100);
          delay(100); 
          noTone(buzzer);
          delay(100 );
       }
  
  } 

}    

void ActivateBomb( char customKey ){


  lcd.print("Setup Code #:");
 
  if( customKey ){
    beeper();
    Data[data_count] = customKey; 
    lcd.setCursor(data_count ,1);
    lcd.print(Data[data_count] );
    data_count++;
    
  }

  if( data_count == Password_lenght - 1  ){
    lcd.clear();

    if( !strcmp(Data, Master) ){ 
      lcd.print("Activated");
      setDeviceState(false);
    } else {
      lcd.print("Declined");   
      delay(1000);
    }

    lcd.clear();
    clearData();
  }

}

void DefuseBomb( char customKey ){
   
  lcd.print("# Code to Defuse it!!!:");
 
  if( customKey ){
    beeper();
    Data[data_count] = customKey; 
    lcd.setCursor(data_count ,1);
    lcd.print(Data[data_count] );
    data_count++;
    
  }

  if( data_count == Password_lenght - 1  ){
    lcd.clear();

    if( !strcmp(Data, Demaster) ){ 
      lcd.print("Bomb Defused");
      tone(buzzer, 100);
      delay(100);
      noTone(buzzer);
      delay(100 );  
      tone(buzzer, 100);
      delay(100);
      noTone(buzzer);
      delay(100 );  
      delay(1000 );  
      setDeviceState(true);
      
    } else {
      lcd.print("Good bye!!!");  
       tone(buzzer, 5000);
       delay(5000);
       noTone(buzzer);  
       delay(1000);
    } 

    lcd.clear();
    clearData();
  }

}


void clearData(){
    while (data_count != 0) {
      Data[data_count--] = 0;
    }
    return;
}

void setDeviceState( boolean isActivate ){
  if( isActivate ){
    ToDefuseSate = false;
    ToActivationState = true;
  } else {
    ToDefuseSate = true;
    ToActivationState = false;
  }
}

void beeper() {
     tone(buzzer, 100);
     delay(100); 
     noTone(buzzer);    
}
