#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Keypad.h>

#define Password_lenght 4

char Data[Password_lenght]; 

char Master[Password_lenght] = "123";

byte data_count = 0;
   
char customKey;

const byte ROWS = 4;
const byte COLS = 3;

char hexaKeys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'},
};

byte rowPins[ROWS] = {9,8,7,6};
byte colPins[COLS]= {5,4,3}; 

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

LiquidCrystal_I2C lcd(0x27,16,2);

void setup() {
  // put your setup code here, to run once:
  //pinMode(13,OUTPUT);
  Serial.begin(9600);
  lcd.backlight();
  lcd.init(); 
}
 
String userInputed = "";
void loop() {

  lcd.setCursor(0,0);
  lcd.print("Setup Code #:");
   
  customKey = customKeypad.getKey();

  if( customKey ){

    Data[data_count] = customKey; 
    lcd.setCursor(data_count ,1);
    lcd.print(Data[data_count] );
    data_count++;
    
  }

  if( data_count == Password_lenght - 1  ){
    lcd.clear();

    if( !strcmp(Data, Master) ){ 
      lcd.print("Activated");
       delay(5000);
    } else {
      lcd.print("Declined");   
      delay(1000);
    }

    lcd.clear();
    clearData();
  }
  
   // lcd.clear();
   // lcd.setCursor(0,0);
   // lcd.print(customKey); 
  //  Serial.println(customKey); 
  /*  userInputed += customKey; 
    Serial.println(userInputed); 

    //Once bomb planted LDC timer count down & blink2x red LED & Sound beeping
    if( userInputed == "123" ){
       Serial.println("Once bomb planted LDC timer count down & blink2x red LED & Sound beeping");
       userInputed = ""; 
    }
    //Turn off red LED and turn on green LED & stop beeping sound
    else if( userInputed == "456"  ){
       Serial.println("Turn off red LED and turn on green LED & stop beeping sound"); 
       userInputed = ""; 
    }*/
    
 
}  

void clearData(){
    while (data_count != 0) {
      Data[data_count--] = 0;
    }
    return;
}